#!/bin/sh

set -e

#PKGOS-INCLUDE#

KEY_CONF=/etc/keystone/keystone.conf

keystone_get_debconf_admin_credentials () {
	db_get keystone/admin-user
	ADMIN_USER_NAME=${RET:-admin}
	db_get keystone/admin-password
	ADMIN_USER_PW=${RET:-$(gen_password)}
	db_get keystone/admin-email
	ADMIN_USER_EMAIL=${RET:-root@localhost}
	db_get keystone/admin-tenant-name
	ADMIN_TENANT_NAME=${RET:-admin}
	db_get keystone/admin-role-name
	ADMIN_ROLE_NAME=${RET:-admin}

	# We export the retrived credentials for later use
	export OS_PROJECT_DOMAIN_ID=default
	export OS_USER_DOMAIN_ID=default
	export OS_USERNAME=admin
	export OS_PASSWORD=${ADMIN_USER_PW}
	export OS_TENANT_NAME=${ADMIN_TENANT_NAME}
	export OS_PROJECT_NAME=${ADMIN_TENANT_NAME}
	export OS_AUTH_URL=http://127.0.0.1:5000/v3/
	export OS_IDENTITY_API_VERSION=3
	export OS_AUTH_VERSION=3
	export OS_PROJECT_DOMAIN_ID=default
	export OS_USER_DOMAIN_ID=default
	export OS_NO_CACHE=1
}

keystone_bootstrap_admin () {
	# This is the new way to bootstrap the admin user of Keystone
	# and we shouldn't use the admin auth token anymore.
	export OS_BOOTSTRAP_USERNAME=${ADMIN_USER_NAME}
	export OS_BOOTSTRAP_PROJECT_NAME=${ADMIN_TENANT_NAME}
	export OS_BOOTSTRAP_PASSWORD=${ADMIN_USER_PW}

	REG_ENDPOINT_IPV4_REGEX='^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$'
	REG_ENDPOINT_IPV6_REGEX="^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$"
	REG_ENDPOINT_FQDN_REGEX='^((([a-z0-9]([-a-z0-9]*[a-z0-9])?)|(#[0-9]+)|(\[((([01]?[0-9]{0,2})|(2(([0-4][0-9])|(5[0-5]))))\.){3}(([01]?[0-9]{0,2})|(2(([0-4][0-9])|(5[0-5]))))\]))\.)*(([a-z]([-a-z0-9]*[a-z0-9])?)|(#[0-9]+)|(\[((([01]?[0-9]{0,2})|(2(([0-4][0-9])|(5[0-5]))))\.){3}(([01]?[0-9]{0,2})|(2(([0-4][0-9])|(5[0-5]))))\]))$'
	REG_ENDPOINT_REGION_REGEX="^([_a-zA-Z0-9]+)([_.a-zA-Z0-9-]*)([_.a-zA-Z0-9]+)\$"


	db_get keystone/register-endpoint
	if [ "$RET" = "true" ] ; then
		do_REGISTER_THE_KS_ENDPOINT=yes
		db_get keystone/region-name
		my_REGION_NAME=${RET}
		REGION_NAME=${RET}

		db_get keystone/endpoint-ip
		# Validate that the choosen endpoint is an IPv4, IPv6 or FQDN
		KEYSTONE_ENDPOINT_IP=`echo "${RET}" | egrep ${REG_ENDPOINT_IPV4_REGEX}` || true
		if [ -z "${KEYSTONE_ENDPOINT_IP}" ] ; then
			KEYSTONE_ENDPOINT_IP=`echo "${RET}" | egrep ${REG_ENDPOINT_IPV6_REGEX}` || true
			if [ -z ${KEYSTONE_ENDPOINT_IP} ] ; then
				KEYSTONE_ENDPOINT_IP=`echo ${RET} | egrep ${REG_ENDPOINT_FQDN_REGEX}` || true
				if [ -z ${KEYSTONE_ENDPOINT_IP} ] ; then
					echo "Keystone's address could not be validated: will not register endpoint."
					do_REGISTER_THE_KS_ENDPOINT=no
				fi
			fi
		fi

		# Validate that the region name has only chars, dashes and dots
		my_REGION_NAME=`echo "${REGION_NAME}" | egrep ${REG_ENDPOINT_REGION_REGEX}` || true
		if [ -z "${my_REGION_NAME}" ] ; then
			echo "This region could not be validated: will not register endpoint."
			do_REGISTER_THE_KS_ENDPOINT=no
		fi

		if [ "${do_REGISTER_THE_KS_ENDPOINT}" = "yes" ] ; then
			db_get keystone/endpoint-proto
			PROTO=${RET}
			BOOTSTRAP_ADDED_PARAMS="--bootstrap-region-id ${REGION_NAME} --bootstrap-admin-url ${PROTO}://${KEYSTONE_ENDPOINT_IP}:5000 --bootstrap-public-url ${PROTO}://${KEYSTONE_ENDPOINT_IP}:5000 --bootstrap-internal-url ${PROTO}://${KEYSTONE_ENDPOINT_IP}:5000"
		else
			BOOTSTRAP_ADDED_PARAMS=""
		fi
	else
		BOOTSTRAP_ADDED_PARAMS=""
	fi
	echo "Now doing: su keystone -s /bin/sh -c 'keystone-manage bootstrap --bootstrap-role-name admin --bootstrap-service-name keystone ${BOOTSTRAP_ADDED_PARAMS}'"
	su keystone -s /bin/sh -c "keystone-manage bootstrap --bootstrap-role-name admin --bootstrap-service-name keystone ${BOOTSTRAP_ADDED_PARAMS}"
}

keystone_create_admin_tenant () {
	echo -n "Fixing-up: admin-project-desc "
	openstack project set --description "Default Debian admin project" $ADMIN_TENANT_NAME
	echo -n "service-project "
	openstack project create --or-show service --description "Default Debian service project" >/dev/null
	echo -n "default-admin-email "
	openstack user set --description "Default Debian admin user" --email ${ADMIN_USER_EMAIL} --enable $ADMIN_USER_NAME
	echo "...done!"

	# Note: heat_stack_owner, heat_stack_user is needed for heat to work, and Member ResellerAdmin
	# are needed for swift auto account creation.
	echo -n "Adding roles: "
	for i in KeystoneAdmin KeystoneServiceAdmin heat_stack_owner \
		heat_stack_user Member ResellerAdmin rating service \
		owner k8s_admin k8s_developer k8s_viewer \
		load-balancer_admin load-balancer_member; do
		echo -n "${i} "
		openstack role create --or-show ${i} >/dev/null
	        # Note: If heat_stack_user role is adding, don't assing it to admin user.
		# This role is automatically assigned by Heat to the users it creates.
	        # This role is restricted from all API access, and it never should be assigned to any user explicitly.
		if [ "${i}" != "heat_stack_user" ]; then
			openstack role add --project $ADMIN_TENANT_NAME --user $ADMIN_USER_NAME ${i} >/dev/null
		fi
	done
	echo "...done!"
}

if [ "$1" = "configure" ] ; then
	. /usr/share/debconf/confmodule
	. /usr/share/dbconfig-common/dpkg/postinst

	# Create user and group keystone, plus /var/log and /var/lib owned by it
	# We need a bash shell so that keystone-manage pkg_setup works, and the
	# Wheezy package doesn't have it, failing upgrades
	pkgos_var_user_group keystone /bin/sh
	# Make sure we have a folder to create certs, that isn't world readable
	mkdir -p /etc/keystone/ssl/certs
	chown keystone:keystone /etc/keystone/ssl/certs
	chmod 750 /etc/keystone/ssl/certs
	chown keystone:keystone /etc/keystone/ssl
	chmod 750 /etc/keystone/ssl

	# Create keystone.conf if it's not there
	pkgos_write_new_conf keystone keystone.conf

	# The on-disk policy file is currently broken for Keystone.
	# The admin bootstraping will not work anymore, due to enforcing of system-scope:all.
	rm -f /etc/keystone/policy.json
#	pkgos_write_new_conf keystone policy.json
#	if ! [ -e /etc/keystone/policy.json ] ; then
#		touch /etc/keystone/policy.json
#		chown 0640 /etc/keystone/policy.json
#		chown root:keystone /etc/keystone/policy.json
#	fi

	OSTACKCLI_PARAMS="--os-url=http://127.0.0.1:5000/v3/ --os-domain-name default --os-identity-api-version=3"

	# Make sure /var/log/keystone/keystone.log is owned by keystone
	# BEFORE any keystone-manage calls.
	chown -R keystone:keystone /var/log/keystone

	# Upgrade or create the db if directed to do so
	db_get keystone/configure_db
	if [ "$RET" = "true" ] ; then
		# Configure the SQL connection of keystone.conf according to dbconfig-common
		pkgos_dbc_postinst ${KEY_CONF} database connection keystone $@
		echo "Running: su keystone -s /bin/sh -c 'keystone-manage db_sync'..."
		su keystone -s /bin/sh -c "keystone-manage db_sync"
	fi

	db_get keystone/create-admin-tenant
	if [ "$RET" = "true" ] ; then
		mkdir -p /etc/keystone/fernet-keys
		chown keystone:keystone /etc/keystone/fernet-keys
		chmod 700 /etc/keystone/fernet-keys
		echo "Running: su keystone -s /bin/sh -c 'keystone-manage fernet_setup --keystone-user keystone --keystone-group keystone'..."
		su keystone -s /bin/sh -c 'keystone-manage fernet_setup --keystone-user keystone --keystone-group keystone'
		echo "Running: su keystone -s /bin/sh -c 'keystone-manage credential_setup --keystone-user keystone --keystone-group keystone'..."
		su keystone -s /bin/sh -c 'keystone-manage credential_setup --keystone-user keystone --keystone-group keystone'
	fi

	chown keystone:adm /var/log/keystone

	if [ -n $(which systemctl)"" ] ; then
		systemctl enable keystone
	fi
	if [ -n $(which update-rc.d)"" ] ; then
		update-rc.d keystone defaults
	fi
	invoke-rc.d keystone start

	db_get keystone/create-admin-tenant
	if [ "$RET" = "true" ] ; then
		echo -n "Sleeping 10 seconds to make sure the keystone daemon is up and running: 10..."
		sleep 1
		echo -n "9..."
		sleep 1
		echo -n "8..."
		sleep 1
		echo -n "7..."
		sleep 1
		echo -n "6..."
		sleep 1
		echo -n "5..."
		sleep 1
		echo -n "4..."
		sleep 1
		echo -n "3..."
		sleep 1
		echo -n "2..."
		sleep 1
		echo -n "1..."
		sleep 1
		echo "0"

		keystone_get_debconf_admin_credentials
		echo "===> Bootstraping tenants with 'keystone-manage bootstrap':"
		keystone_get_debconf_admin_credentials
		keystone_bootstrap_admin
		db_unregister keystone/register-endpoint
		echo "===> Editing bootstraped tenants and adding default roles"
		keystone_create_admin_tenant
		echo "done!"
	fi
	db_unregister keystone/create-admin-tenant
	db_stop
fi

exit 0
